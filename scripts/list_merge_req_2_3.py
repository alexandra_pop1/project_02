import requests
import urllib
import re
import json

# access merge request
url1 = "https://gitlab.com/api/v4/projects/21992132/merge_requests?id=21992132&page=1&per_page=5000&"
response = requests.head(url1, headers={"PRIVATE-TOKEN": "UoM_4vP5zwX9MXypwFDD"})

data1 = urllib.request.urlopen(url1).read()
show_merge_request = str(data1)

# count merge requests
merge_request_number = show_merge_request.count("merge_requests")
print("This project has:\t",merge_request_number,"\tmerge requests")
print()

# extract detail from merge requests
merge_link = re.findall('{"short":"!\d+","\w+":"!\d+","full":"\w+\/\w+!\d+"},"\w+":"\w+:\/\/\w+.\w+\/\w+\/\w\w+\/-\/merge_requests\/\d+"',show_merge_request)
merge_branch_data = re.findall('"target_branch":"\w+","source_branch":"\w+"',show_merge_request)
merge_create_data = re.findall('"created_at\":\"\d+-\d+-\w+:\d+:\d+',show_merge_request)
merge_close_data = re.findall('"closed_at":"\w+-\w+-\w+:\w+:\w+.\w+"|"closed_at":null',show_merge_request)

for a,b,c,d in zip(merge_link,merge_branch_data,merge_create_data,merge_close_data):
    print(a,b,c,d)


# use json to read tha data
# a = show_merge_request
# a = a.replace(a[:1],"")
# a = a.replace(a[len(a) - 1],"")
# parsed = json.loads(a)
# a = json.dumps(parsed, indent=4, sort_keys=True)
# print(a)
