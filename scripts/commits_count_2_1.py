import requests
import urllib
import re

def clean_data(a):
    a = str(a)
    a = a.replace("[","")
    a = a.replace("]","")
    a = a.replace("\"","")
    a = a.replace("'","")
    a = a.replace("name:","")
    a = a.replace(", ",",")
    a = a.split(",")
    return a

########################################################################################################################

url0 = "https://gitlab.com/api/v4/projects/21992132/users"
response = requests.head(url0, headers={"PRIVATE-TOKEN": "UoM_4vP5zwX9MXypwFDD"})

data0 = urllib.request.urlopen(url0).read()
data0 = str(data0)
pattern0 = '\"name\":\"\w+\ \w+\"'
users = re.findall(pattern0, data0)
users = clean_data(users)

print("Avaiable users: ",users)

########################################################################################################################
url1 = "https://gitlab.com/api/v4/projects/21992132/repository/branches"
response = requests.head(url1, headers={"PRIVATE-TOKEN": "UoM_4vP5zwX9MXypwFDD"})

data1 = urllib.request.urlopen(url1).read()
data1 = str(data1)
no_of_commits = data1.count('"short_id"')

# prepare branches data for interogate
pattern = '\"name\":\".\w+\"'
branches = re.findall(pattern,data1)
branches = clean_data(branches)

for branch_name in branches:
    url2 = "https://gitlab.com/api/v4/projects/21992132/repository/commits?id=21992132&page=1&per_page=5000&ref_name="+branch_name
    response = requests.head(url2, headers={
        "PRIVATE-TOKEN": "UoM_4vP5zwX9MXypwFDD"})
    data2 = urllib.request.urlopen(url2).read()
    data2 = str(data2)

    # print(data2)
    no_of_commits = data2.count('"short_id"')
    print("the branch: ",branch_name, "have: ", no_of_commits, "commits")